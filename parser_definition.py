import argparse
import config
import datetime as dt


class _HelpAction(argparse._HelpAction):
    """
    Create class to override the default --help argument from argparse to handle
    subparser main help.

    Source: https://stackoverflow.com/questions/20094215/argparse-subparser-monolithic-help-output
    """

    def __call__(self, parser, namespace, values, option_string=None):
        parser.print_help()

        # retrieve subparsers from parser
        subparsers_actions = [
            action for action in parser._actions
            if isinstance(action, argparse._SubParsersAction)]
        # there will probably only be one subparser_action,
        # but better safe than sorry
        for subparsers_action in subparsers_actions:
            # get all subparsers and print help
            for choice, subparser in subparsers_action.choices.items():
                print("Subparser '{}'".format(choice))
                print(subparser.format_help())

        parser.exit()


def initialise_parser():
    """
    Define argument parser and run specific function required.

    """

    parser = argparse.ArgumentParser('Data handler', add_help=False)

    parser.add_argument('--help', action=_HelpAction)

    parser.add_argument("--input", type=str, required=True, action='store',
                        help="set input parent folder")
    parser.add_argument("--output", type=str, required=True, action='store',
                        help="set output parent folder")

    subparsers = parser.add_subparsers(dest='command', help='Select one of the commands:')

    # sample parser and arguments
    sample_parser = subparsers.add_parser('sample', help='sample subreddit for images')

    sample_parser.add_argument('--subreddit', type=str, required=True, help='Subreddit to sample.',
                               action='store')
    sample_parser.add_argument('--client_id', type=str, required=False, action='store',
                               default=config.client_id,
                               help="Set client id for praw. DEFAULT: client_id in config.py")
    sample_parser.add_argument('--client_secret', type=str, required=False, action='store',
                               default=config.client_secret,
                               help="Set client secret for praw. DEFAULT: client_secret in config.py")
    sample_parser.add_argument('--limit', type=int, required=False, action='store',
                               default=5000,
                               help="Limit for the number of posts to sample.")
    sample_parser.add_argument('--start_date', type=str, required=False, action='store',
                               default=(dt.datetime.now() - dt.timedelta(days=7)).strftime('%Y-%m-%d'),
                               help="Start date [yyyy-mm-dd] to sample from. DEFAULT: 7 days before today.")
    sample_parser.add_argument('--end_date', type=str, required=False, action='store',
                               default=dt.datetime.today().strftime('%Y-%m-%d'),
                               help="End date [yyyy-mm-dd] to sample to. DEFAULT: today.")
    sample_parser.add_argument('--reduce_size_above', type=int, required=False, action='store',
                               default=2000,
                               help="Will crop images above this to 1200x1200. DEFAULT: >2000 pixels wide.")
    sample_parser.add_argument('--identify-colour', dest='colour', action='store_true')
    sample_parser.add_argument('--no-identify-colour', dest='colour', action='store_false')
    sample_parser.set_defaults(feature=True)

    # preprocess parser and arguments
    preprocess_parser = subparsers.add_parser('preprocess', help='preprocess images for training')

    preprocess_parser.add_argument('--bw', help='Convert images to bw. DEFAULT: True', action='store_true', dest='bw')

    preprocess_parser_image_size = preprocess_parser.add_mutually_exclusive_group()
    preprocess_parser_image_size.add_argument('--resize',
                                              help='Resize images to specified size eg. 224 224. DEFAULT: None',
                                              action='store', default=None, nargs='+', type=int)
    preprocess_parser_image_size.add_argument('--patch_size',
                                              help='Sample images based on specified size eg. 224 224. DEFAULT: None',
                                              action='store', default=None, nargs='+', type=int)
    preprocess_parser_image_size.add_argument('--training_split',
                                              help='Percentage of events to use for training. DEFAULT: 0.7',
                                              action='store', default=0.7, type=float)

    # add preprocessing signal options
    preprocess_parser_signal_group = preprocess_parser.add_mutually_exclusive_group()

    preprocess_parser_signal_group.add_argument('--emd', help='Apply EMD to image. DEFAULT: False',
                                                action='store_true', dest='emd')
    preprocess_parser_signal_group.add_argument('--wavelet', help='Apply wavelet transform to image. DEFAULT: False',
                                                action='store_true', dest='wavelet')
    preprocess_parser_signal_group.add_argument('--laplacian',
                                                help='Apply laplacian (cv2.CV_64F, kernel=7) to image. DEFAULT: False',
                                                action='store_true', dest='laplacian')
    preprocess_parser_signal_group.add_argument('--hpf', help='Apply high pass filter to image. DEFAULT: False',
                                                action='store_true', dest='hpf')

    # add image modification options
    preprocess_parser_image_mod = preprocess_parser.add_mutually_exclusive_group()

    preprocess_parser_image_mod.add_argument('--subsample', help='Randomly sample n patches. DEFAULT: None',
                                             action='store', dest='subsample', type=int, default=None)
    preprocess_parser_image_mod.add_argument('--top_n_patches', help='Select top n patches. DEFAULT: None',
                                             action='store', dest='top_n_patches', type=int, default=None)

    # generate training csv for data generator
    data_generation_parser = subparsers.add_parser('trainfile', help='create training file for csv')

    data_generation_parser.add_argument('--csv_path', help='Path to csv file to write to. DEFAULT: None.',
                                        action='store', dest='csv_path', type=str, default=None)

    return parser
