#!/usr/bin/env python

from classes.DataScraper import DataScraper
from classes.ImageProcessor import ImageProcessor
import parser_definition


def main():
    """
    Define argument parser and run specific function required.

    """

    parser = parser_definition.initialise_parser()
    args = parser.parse_args()

    if args.command == 'sample':
        # initialise data scraper class with argparse arguments
        dh = DataScraper(
            input_folder=args.input,
            output_folder=args.output,
            client_id=args.client_id,
            client_secret=args.client_secret
        )

        # sample specified subreddit with chosen parameters
        dh.sample_subreddit(
            subreddit=args.subreddit,
            start_date=args.start_date,
            end_date=args.end_date,
            lim=args.limit,
            reduce_size_above=args.reduce_size_above,
            identify_colour=args.colour
        )

    else:
        ip = ImageProcessor(
            input_folder=args.input,
            output_folder=args.output
        )

        if args.command == 'preprocess':

            if args.resize:
                args.resize = tuple(args.resize)

            ip.process_images(
                bw=args.bw,
                resize=args.resize,
                emd=args.emd,
                laplacian=args.laplacian,
                hpf=args.hpf,
                wavelet=args.wavelet,
                training_split=args.training_split,
                pick_best_patch=args.top_n_patches,
                patch_size=args.patch_size
            )

        if args.command == 'trainfile':

            ip.generate_training_csv(
                output_name=args.csv_path
            )


if __name__ == "__main__":
    main()
