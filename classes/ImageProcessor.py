import glob
import math
import os
from operator import itemgetter
import cv2
import pandas as pd
import pywt
from PIL import Image, ImageChops
import numpy as np
from scipy import ndimage
from PyEMD.EMD2d import EMD2D
import random

from tqdm import tqdm


class ImageProcessor:
    """
    Class for processing images to be used for training our models

    """

    def __init__(self, input_folder, output_folder):
        """
        Initialise our image processing class

        :param input_folder: the location of the images we want to process
        :type input_folder: str
        :param output_folder: the location where we want to output process images
        :type output_folder: str
        """
        self.input_folder = input_folder
        self.output_folder = output_folder

    @staticmethod
    def _trim_borders(im, threshold=-100):
        """
        This method attempts to trim the border around an image based on the pixel value in the top left corner
        and a certain threshold
        base: trim from: https://stackoverflow.com/questions/10615901/trim-whitespace-using-pil/10616717#10616717

        TODO:
            possibly take an average of 4 corners to better approximate border colour

        :param im: image we want to trim the borders for
        :type im: ndarray
        :param threshold: threshold for removing noise
        :type threshold: int
        :return: returns the image as an array with border removed
        :rtype: ndarray
        """

        im = Image.fromarray(im)

        # sample top left pixel for border approximation
        bg = Image.new(im.mode, im.size, im.getpixel((0, 0)))

        # reduce for noise by subtracting threshold value from pixel values - also allows us to work for b+b/w borders
        diff = ImageChops.difference(im, bg)
        diff = ImageChops.add(diff, diff, 2.0, threshold)

        # calculate the bounding box
        bbox = diff.getbbox()

        if bbox:
            return np.asarray(im.crop(bbox))
        else:
            return np.asarray(im)

    @staticmethod
    def _high_pass_filter(img):
        """
        Applies highpass filter to image.
        Kernel sourced from:

        TODO:
            - remove color boolean - we can calculate this based on array shape - DONE

        :param img: image to apply hpf to
        :type img: ndarray
        :return: image with high pass filter applied
        :rtype: ndarray
        """
        kernel = np.array([[-1, 2, -2, 2, -1],
                           [2, -6, 8, -6, 2],
                           [-2, 8, -12, 8, -2],
                           [2, -6, 8, -6, 2],
                           [-1, 2, -2, 2, 1]])

        if img.ndim == 3:
            return ndimage.convolve(img, np.dstack([kernel, kernel, kernel]) * (1.0 / 12.0))
        else:
            return ndimage.convolve(img, kernel * (1.0 / 12.0))

    def _prepare_image(self, img, hpf=True, color=False, resize=None, wavelet=False,
                       laplacian=False):
        """
        Apply preprocessing that is selected to image

        :param img: image we want to process
        :type img: ndarray
        :param hpf: whether to apply high pass filter
        :type hpf: bool
        :param color: whether image is colour or not
        :type color: bool
        :param resize: image dimensions to resize to
        :type resize: tuple
        :param wavelet: whether to apply wavelet decomposition to image
        :type wavelet: bool
        :param laplacian: whether to apply laplacian to image
        :type laplacian: bool

        :return: processed image
        :rtype: ndarray
        """

        if len(img) > 0:
            if hpf:
                img = self._high_pass_filter(img, color=color)
            if wavelet:
                img = self._apply_wavelet_transform(img)
            if laplacian:
                img = self._return_laplacian(img)
            if resize:
                img = cv2.resize(img, resize)

            return img

        return None

    @staticmethod
    def _return_laplacian(img, ddepth=cv2.CV_16S, kernel=5):
        """

        :param img: image to process
        :type img: ndarray
        :param ddepth: define depth to use (number of bits per channel)
        :type ddepth: cvtype (DEFAULT: cv2.CV_8U)
        :param kernel: kernel size
        :type kernel: int
        :return: processed image
        :rtype: ndarray
        """

        return cv2.Laplacian(img, ddepth, ksize=kernel)

    @staticmethod
    def _return_emd_residual(img):
        """

        :param img: image to process
        :type img: ndarray
        :return: processed image
        :rtype: ndarray
        """

        emd2d = EMD2D()

        if img.ndim == 2:
            img = np.dstack([img, img, img])

        r, g, b = cv2.split(img)

        _r = emd2d(r)[0]
        _g = emd2d(g)[0]
        _b = emd2d(b)[0]

        imf = cv2.merge((_r.astype(int), _g.astype(int), _b.astype(int)))

        return ((imf - imf.min()) * (1 / (imf.max() - imf.min()) * 255)).astype('uint8')

    def _return_best_patches(self, imgs, top_n_patches=1, threshold=0):
        """
        Iterate through list if images and calculate image quality, and return top n best patches

        :param imgs: list of images
        :type imgs: list
        :param top_n_patches: number of best patches to calculate
        :type top_n_patches: int
        :param threshold: the minimum threshold of image quality to use (DEFAULT: 0).
        :type threshold: float
        :return: list of top n images which are above threshold
        :rtype: list
        """

        patch_pairs = []

        for i, _img in enumerate(imgs):
            q = self._image_quality(_img)
            if q > threshold:
                patch_pairs.append([_img, q])

        if len(patch_pairs) < top_n_patches:
            return imgs

        patch_pairs.sort(key=itemgetter(1), reverse=True)

        return [i[0] for i in patch_pairs[0:top_n_patches]]

    @staticmethod
    def _image_quality(img, alpha=0.7, beta=4.0, gamma=np.log(0.01)):
        """
        Calculate quality of the image based on -> This quality measure tends to be lower for overly saturated or
            flat patches, whereas it is higher for textured patches showing some statistical variance

        Source: https://arxiv.org/pdf/1809.00576.pdf

        :param img: image to calculate quality ofr
        :type img: ndarray
        :param alpha: constant
        :type alpha: float
        :param beta: constant
        :type beta: float
        :param gamma: constant
        :type gamma: float
        :return: calculated image quality
        :rtype: float
        """

        return sum([(1 / 3.) * (
                alpha * beta * (np.mean((1 / 255.) * c) - (np.mean((1 / 255.) * c)) ** 2.0) + (1 - alpha) * (
                1 - np.e ** (gamma * np.std((1 / 255.) * c)))) for c in cv2.split(img)])

    @staticmethod
    def _return_wavelet_coefficients(im, type='bior1.3'):
        """
        Calculate the wavelet coefficients using 2d discrete wavelet transform
        See more: https://pywavelets.readthedocs.io/en/latest/regression/wavelet.html

        :param im: image to calculate wavelet coefficients for
        :type im: ndarray
        :param type: wavelet family to use (DEFAULT: Biorthogonal 1.3 wavelet)
        :type type: str
        :return: Approximation, horizontal detail, vertical detail and diagonal detail coefficients respectively.
        :rtype: tuple
        """

        coeffs2 = pywt.dwt2(im, type)
        LL, (LH, HL, HH) = coeffs2

        return LL, LH, HL, HH

    def _apply_wavelet_transform(self, im, direction='HH'):
        """

        :param im: image to apply wavelet transform to
        :type im: ndarray
        :param direction: Approximation, horizontal detail, vertical detail and diagonal
            detail coefficients respectively [LL, LH, HL, HH] : (DEFAULT: HH)
        :type direction: str
        :return: transformed image
        :rtype: ndarray
        """
        dir = {'LL': 0, 'LH': 1, 'HL': 2, 'HH': 3}

        coeffs = self._return_wavelet_coefficients(im)

        return np.uint8(coeffs[dir[direction]])

    @staticmethod
    def _return_dynamic_range(img):
        """

        :param img: image to get dybamic range of
        :type img: ndarray
        :return: dynamic range of image
        :rtype: float
        """

        th99 = np.percentile(img, 99)
        th01 = np.percentile(img, 1)

        return np.log2(th99+0.01) - np.log2((0.01 + th01))

    @staticmethod
    def signaltonoise(img, axis=None, ddof=0):
        """

        :param img: image to calculate s2n ratio of
        :type img: ndarray
        :param axis: which axis to use for calculating signal
        :type axis: int
        :param ddof: delta degrees of freedom
        :type ddof: int
        :return:
        :rtype:
        """

        a = np.asanyarray(img)
        m = a.mean()
        sd = a.std(axis=axis, ddof=ddof)
        return np.where(sd == 0, 0, m / sd)

    @staticmethod
    def _return_image_type(_path, expected_position=-3):
        """
        Identify whether image is colour/bw from directory structure generated by DataScraper

        :param _path: path of the image
        :type _path: str
        :param expected_position: expected position of type
        :type expected_position: int
        :return: returns the type if possible
        :rtype: str
        """

        try:
            return _path.split("/")[expected_position]
        except IndexError:
            print("File structure not as expected from data scraper, aborting...")
            return None

    def generate_training_csv(self, output_name=None, parent_image_folder = './images/') -> None:
        """
        Generate dataframe for training models using our own data generator

        TODO:
            handle patch/base image processing in separate function; store information in json files to stop reprocessing

        :param parent_image_folder:
        :type parent_image_folder:
        :param output_name: name of output csv
        :type output_name: str
        :return:
        :rtype:
        """

        if not output_name:
            output_name = self.output_folder

        df_train = {}

        base_image_paths = {name: os.path.join(path, name) for path, subdirs, files in os.walk(parent_image_folder) for name in files}

        for i, _path in enumerate(glob.glob(self.input_folder + '**/*.png', recursive=True)):

            label = self._return_image_type(_path, expected_position=-2)
            ml_kind = self._return_image_type(_path, expected_position=-3)

            if not label:
                continue

            try:

                _subpath, _name = os.path.abspath(_path).rsplit("/", 1)

                # check if we used patch generation to create images, if we did normalise name to find base image
                base_image_name = _name
                if "top" in self.input_folder:
                    base_image_name = _name.rsplit("_",1)[0]+".png"

                # we cant seem to find parent non-processed image
                if base_image_name not in base_image_paths:
                    print("Skipping, can't find base image.")
                    continue
                img = cv2.imread(base_image_paths[base_image_name])

                snr = float(self.signaltonoise(img))
                dr = float(self._return_dynamic_range(img))
                LL, LH, HL, HH = self._return_wavelet_coefficients(img)
            except:
                print("Failed to generate data for: " + str(_path))
                continue

            df_train[_name] = {'label': label, 'path':_subpath,'ml_kind': ml_kind,
                               'wavelet_coefficients': [LL.var(), LH.var(), HL.var(), HH.var()],
                               'signal_to_noise_ratio': snr, 'dynamic_range': dr, 'LH_var': LH.var(),
                               'meta_features': [snr, LH.var(), dr]}

        df = pd.DataFrame.from_dict(df_train, orient='index')
        df = df.reset_index().rename(columns={'index': 'name'})

        df.to_csv(output_name, index=False)

    @staticmethod
    def _split_image(img, xy=(224, 224)) -> list:
        """

        :param img: image to split
        :type img: ndarray
        :param xy: size of patches to split image into
        :type xy: tuple
        :return: list of patches that image has been split into
        :rtype: list
        """

        imgs, xsplit, ysplit = [], xy[0], xy[1]

        for y in range(1, math.floor(img.shape[0] / ysplit) + 1):
            for x in range(1, math.floor(img.shape[1] / xsplit) + 1):
                imgs.append(img[int((y - 1) * ysplit):int(y * ysplit), int((x - 1) * xsplit):int(x * xsplit)])

        return imgs

    def process_images(self, training_split=0.8, pick_best_patch=0, subsample=0, bw=False,
                       emd=False, laplacian=False, hpf=False, wavelet=False, resize=None, patch_size=None):
        """

        TODO:
            - handle training split with training from dataframe not splitting at processing time
            - remove/clean up subsampling
            - remove folder structure generation into sep function

        :param training_split: split between val and train
        :type training_split: float
        :param pick_best_patch: number of best patches to select
        :type pick_best_patch: int
        :param subsample: random subsampling of patches
        :type subsample: int
        :param bw: convert to bw
        :type bw: bool
        :param emd: apply emd
        :type emd: bool
        :param laplacian: apply laplacian
        :type laplacian: bool
        :param hpf: apply high pass filter
        :type hpf: bool
        :param wavelet: apply wavelet decomposition
        :type wavelet: bool
        :param resize: resize image
        :type resize: tuple

        :return:
        :rtype:
        """

        # create expected output structure for keras training
        output_structure = {self.output_folder.split("/")[-1]:
            {'train': {
                'digital': {},
                'film': {}
            },
                'val': {
                    'digital': {},
                    'film': {}
                }
            }
        }

        # recursively generate folders we need
        def generate_output_structure(file_structure, dir=''):
            for k, v in file_structure.items():
                it_dir = os.path.join(dir, k)
                if not os.path.isdir(it_dir):
                    os.mkdir(it_dir)
                if type(v) == dict:
                    generate_output_structure(v, os.path.join(dir, k))

        generate_output_structure(output_structure, self.output_folder.rsplit("/", 1)[0])

        # check if output dir exists
        if not os.path.isdir(self.output_folder):
            os.makedirs(self.output_folder)

        # check which images have been processed
        processed_images = [name for path, subdirs, files in os.walk(self.output_folder) for name in files]

        for i, _path in tqdm(enumerate(glob.glob(self.input_folder + '**/*.png', recursive=True))):

            # determine name and type of image [digital, colour]
            _name = os.path.basename(_path)

            # decide on train/val split
            _split = 'train'
            if random.randint(0, 100) / 100. > training_split:
                _split = 'val'

            # get whether image is col/bw
            _type = self._return_image_type(_path)
            if not _type:
                continue

            # skip if image is already processed
            if _name in processed_images:
                continue

            # read foundation image
            base_image = cv2.imread(_path)

            if bw:
                base_image = cv2.cvtColor(base_image, cv2.COLOR_RGB2GRAY)

            # apply processing to base image
            base_image = self._prepare_image(np.array(base_image, dtype=float), hpf=hpf, resize=resize, wavelet=wavelet,
                                             laplacian=laplacian)

            # check if we want to subsample image or pick n patches
            if subsample or pick_best_patch > 0:

                _imgs = self._split_image(base_image, xy=patch_size)

                # if we want to randomly subsample, select random n patches
                if subsample > 0:
                    if len(_imgs) > subsample:
                        _imgs = random.sample(_imgs, subsample)
                    else:
                        continue

                # if we wanted to pick best n patches
                elif pick_best_patch > 0:
                    _imgs = self._return_best_patches(_imgs, top_n_patches=pick_best_patch)

                for c, im in enumerate(_imgs):
                    if emd:
                        im = self._return_emd_residual(im)

                    cv2.imwrite(os.path.join(self.output_folder, _split, _type,
                                             _name.replace(".png", "") + "_" + str(c) + ".png"), im)

            else:
                if emd:
                    base_image = self._return_emd_residual(base_image)

                cv2.imwrite(os.path.join(self.output_folder, _split, _type, _name), base_image)
