import datetime as dt
import cv2
from skimage import io
from tqdm import tqdm
import os
import string
import praw
from pmaw import PushshiftAPI
import time
import random
import skimage
import urllib

valid_chars = "-_() %s%s" % (string.ascii_letters, string.digits)


class DataScraper:
    """
    Class for scraping images from specific subreddits

    """

    def __init__(self, input_folder, output_folder, client_id=None, client_secret=None, client=None):
        """
        Initialise data scraper class with input/output folders and class secret for reddit

        :param input_folder: the location of the folder where images are already stored so we can avoid duplicates
        :type input_folder: str
        :param output_folder: the location of the folder where we want to store scraped images
        :type output_folder: str
        :param client_id: client id
        :type client_id: str
        :param client_secret: client secret
        :type client_secret: str
        :param client: initialised PushshiftAPI client to use for scraping data from reddit
        :type client: PushshiftAPI
        """
        self.input = input_folder
        self.output = output_folder
        self.client_id = client_id
        self.client_secret = client_secret
        self.client = client

    @staticmethod
    def _is_bw(img, pixel_threshold=3, change_threshold=0.20):
        """
        Returns true if image is a b/w image based on a RGB difference of pixels within a certain threshold

        :param img: img array
        :type img: ndarray
        :param pixel_threshold: the threshold for determining that 2 channels pixel values are different
        :type pixel_threshold: int
        :param change_threshold: the percentage of pixels (as % of w*h) that are different
        :type change_threshold: float
        :return: returns true if image is bw (pixel delta is below expected threshold)
        :rtype: bool
        """

        r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]

        d_bg = b - g
        d_br = b - r
        d_rg = r - g

        d_channel = ((d_bg > pixel_threshold) + (d_br > pixel_threshold) + (d_rg > pixel_threshold)).sum()

        return d_channel / (img.shape[0] * img.shape[1]) < change_threshold

    @staticmethod
    def _format_string(string_name):
        """
        Normalise string to use only valid characters, lower case, and replace space with _

        :param string_name: string we want to format
        :type string_name: str
        :return: returns string which only has valid characters
        :rtype: str
        """
        return ''.join(c for c in string_name if c in valid_chars).replace(" ", "_").lower()

    @staticmethod
    def _retrieve_image(img_url):
        """
        Load image from url using skimage library

        :param img_url: url for image
        :type img_url: str
        :return: image as array
        :rtype: ndarray
        """
        try:
            return skimage.io.imread(img_url)
        except (ValueError, urllib.error.HTTPError) as e:
            print("Unexpected error when loading image.")
            return []

    @staticmethod
    def _crop_image(img, crop_width=1200):
        """

        :param img: image we want to crop
        :type img: ndarray
        :param crop_width: the width to which we want to crop the image to
        :type crop_width: int
        :return: cropped image
        :rtype: ndarray
        """

        ps = int(crop_width/2.)
        cy = int(img.shape[0] / 2.0)
        cx = int(img.shape[1] / 2.0)
        return img[cy - ps:cy + ps, cx - ps:cx + ps]

    def sample_subreddit(self, subreddit, start_date, end_date, submissions=None, client=None, reduce_size_above=2000,
                         lim=500, identify_colour=True):
        """

        :param subreddit: subreddit to sample from
        :type subreddit: str
        :param start_date: start date for submission search in format yyyy-mm-dd
        :type start_date: str
        :param end_date: end date for submission search in format yyyy-mm-dd
        :type end_date: str
        :param submissions: list of submissions returned by pushshift api
        :type submissions: list
        :param client: PushshiftAPI client to use for searching submissions
        :type client: PushshiftAPI
        :param reduce_size_above: the width of an image above which we will reduce during scraping
        :type reduce_size_above: int
        :param lim: number of submissions to return
        :type lim: int
        :param identify_colour: if selected will attempt to identify whether image is bw or colour
        :type identify_colour: bool

        :return:
        :rtype:
        """

        if not client:
            # initialise pushshift api with praw client to have more submission information
            reddit = praw.Reddit(client_id=self.client_id, client_secret=self.client_secret, user_agent='filmnotfilm')
            self.client = PushshiftAPI(praw=reddit)

        if not submissions:
            # define start and end time based on str input
            start_epoch = int(dt.datetime.strptime(start_date, '%Y-%m-%d').timestamp())
            end_epoch = int(dt.datetime.strptime(end_date, '%Y-%m-%d').timestamp())

            # generate submission list which matches critera
            _submissions = list(
                self.client.search_submissions(subreddit=subreddit, after=start_epoch, before=end_epoch, limit=lim))
        else:
            _submissions = submissions

        # create reference point for existing images
        _existing_images = [name for path, subdirs, files in os.walk(self.input) for name in files]

        for submission in tqdm(_submissions):

            # do not scrape images if they are a text post, over18 post or have been removed
            if not submission['is_self']:

                if submission['over_18']:
                    continue

                if submission['removed_by_category'] is not None:
                    continue

                if 'gallery_data' in submission:
                    continue

                submission_url = submission['url']
                submission_title = submission['title']

                # clean up name
                _img_name = self._format_string(submission_title) + ".png"

                if _img_name in _existing_images:
                    continue

                # retrieve image and reduce size if above threshold
                _img = self._retrieve_image(submission_url)

                if len(_img) > 0:
                    if reduce_size_above:
                        if _img.shape[0] > reduce_size_above:
                            _img = self._crop_image(_img)

                    _processed_img = cv2.cvtColor(_img, cv2.COLOR_BGR2RGB)

                    # identify image is colour or bw
                    col = ''
                    if identify_colour:
                        col = '/colour/'
                        if self._is_bw(_processed_img):
                            col = '/bw/'

                    cv2.imwrite(self.output + col + _img_name, _processed_img)

                    time.sleep(random.randint(1000, 1300) / 1000.0)
