# base: https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
# https://mahmoudyusof.github.io/facial-keypoint-detection/data-generator/

import numpy as np
from skimage import io
import ast
import keras
import os


class DataGenerator(keras.utils.all_utils.Sequence):
    """
    Create own data generator to handle multiple inputs from df
    """

    def __init__(self, dataset, list_IDs, labels, batch_size=64, dim=(224, 224), n_channels=3,
                 n_classes=2, shuffle=True):
        """

        :param dataset: dataframe for training
        :type dataset: pd.DataFrame
        :param list_IDs: list of ids to generate data from
        :type list_IDs: list
        :param labels: dictionary of image name -> label
        :type labels: dict
        :param batch_size: batch size
        :type batch_size: int
        :param dim: image dimensions
        :type dim: tuple
        :param n_channels: number of image channels
        :type n_channels: int
        :param n_classes: number of training classes
        :type n_classes: int
        :param shuffle: shuffle training data
        :type shuffle: bool
        """

        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.dataset_ref = dataset
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        # 'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        """
        Generate batch size amount of data

        :param index: index for batch
        :type index: int
        :return: returns tuple of training object and label
        :rtype: tuple
        """
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def on_epoch_end(self):
        """
        Shuffle data and update indexes with each epoch

        """
        # 'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        """

        :param list_IDs_temp: list of images we want to process/generate data for
        :type list_IDs_temp: list
        :return: returns tuple of objects and labels
        :rtype: tuple
        """

        # 'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        X = np.empty((self.batch_size, *self.dim, self.n_channels))

        # prepare 2nd input of data
        Xmeta = np.empty((self.batch_size, 3))

        # prepare labels
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):

            _path = os.path.join(self.dataset_ref.loc[str(ID)]['path'], str(ID))

            x_i = (1 / 255.) * io.imread(_path)
            if x_i.ndim != self.n_channels:
                x_i = np.dstack([x_i]*self.n_channels)

            X[i] = x_i

            # set meta features
            Xmeta[i] = ast.literal_eval(str(self.dataset_ref.loc[str(ID)]['meta_features']))

            # Store class
            y[i] = self.labels[str(ID)]

            # print(ID, self.labels[str(ID)])

        if self.n_classes > 2:
            y = keras.utils.to_categorical(y, num_classes=self.n_classes, dtype='float32')
        else:
            y = np.asarray(y, dtype='float32')

        return [X, Xmeta], y
