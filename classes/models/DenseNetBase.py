# Import the Sequential model and layers
import keras
import tensorflow as tf
from keras.layers import Activation, Dropout, Flatten, Dense
import pandas as pd
from sklearn import preprocessing
import sys

#some not-so-nice hacks to read separate module... means if this py file is ran as a script needs to be ran from its directory
sys.path.append(".")

from classes.DataGenerator import DataGenerator

size = 256
batch_size = 8

class DenseNetBase(tf.keras.Model):
    
    def __init__(self, size):
        
        super(DenseNetBase, self).__init__()
        
        #base
        self.base = tf.keras.applications.DenseNet201(weights=None,include_top=False, pooling='avg',input_shape = (size,size,3))
        
        #final layer
        self.dense = Dense(1, activation='sigmoid', name='predictions')
        
    def call(self, input_tensor):
        
        input_image = input_tensor[0]
        input_metafeatures = input_tensor[1]
        
        # model
        x = self.base(input_image)
        
        return self.dense(x)
    
    def build_graph(self):
        x = self.base.input
        y = tf.keras.Input(shape=(3,))
        return tf.keras.Model(inputs=[x,y], outputs=self.call([x,y]))

dataset = pd.read_csv('/home/jan/Documents/python/git/filmnotfilm/training_data/training_csvs/training_file_20220607_grayscale_256_hpf.csv')

dataset.set_index('name', inplace=True)

partition = {}
partition['train'] = dataset[dataset['ml_kind'] == 'train'].index.values
partition['validation'] = dataset[dataset['ml_kind'] == 'val'].index.values

le = preprocessing.LabelEncoder()
encoded_labels = le.fit_transform(dataset.label)

labels = pd.Series(encoded_labels,index=dataset.index).to_dict()

#Parameters
params = {'dim': (size,size),
          'batch_size': batch_size,
          'n_classes': 2,
          'n_channels': 3,
          'shuffle': True}

#Generators
training_generator = DataGenerator(dataset, partition['train'], labels, **params)
validation_generator = DataGenerator(dataset, partition['validation'], labels, **params)

denseBase = DenseNetBase(size)
denseBase.compile(tf.optimizers.Adam(lr=1e-5), 'binary_crossentropy', metrics=['accuracy'])

denseBase.fit(training_generator, validation_data=validation_generator,epochs = 15, verbose =1)

# Evaluating model performance on Testing data
loss, accuracy = denseBase.evaluate(validation_generator)

print("\nModel's Evaluation Metrics: ")
print("---------------------------")
print("Accuracy: {} \nLoss: {}".format(accuracy, loss))