# Import the Sequential model and layers
from keras.models import Sequential
import keras
import tensorflow as tf
from keras.layers import Conv2D, MaxPooling2D, Lambda, Dropout, Dense, GlobalAveragePooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from pathlib import Path

size = 256
batch_size = 16


class SimpleSequentialModel(tf.keras.Model):

    def __init__(self, size):
        super(SimpleSequentialModel, self).__init__()

        self.conv1 = Conv2D(48, (7, 7), input_shape=(size, size, 3))
        self.act1 = Activation('relu')
        self.pool1 = MaxPooling2D(3)
        self.l1 = Lambda(tf.nn.local_response_normalization)

        self.pool2 = GlobalAveragePooling2D()

        self.dense1 = Dense(1024)
        self.act2 = Activation('relu')

        self.flat = Flatten()

        self.drop = Dropout(0.5)

        self.dense2 = Dense(64, activation='relu')
        self.drop2 = Dropout(0.5)

        self.dense3 = Dense(1, activation='sigmoid')

    def call(self, input_tensor):
        x = self.conv1(input_tensor)
        x = self.act1(x)
        x = self.pool1(x)
        x = self.l1(x)

        x = self.pool2(x)

        x = self.dense1(x)
        x = self.act2(x)

        x = self.flat(x)

        x = self.drop(x)

        x = self.dense2(x)
        x = self.drop2(x)

        return self.dense3(x)


ssmodel = SimpleSequentialModel(size)

ssmodel.compile(loss='binary_crossentropy',
                optimizer=tf.optimizers.Adam(learning_rate=1e-5),
                metrics=['accuracy'])

# Training Augmentation configuration
from keras.preprocessing.image import ImageDataGenerator

base_dir = Path(__file__).resolve().parents[2]

train_datagen = ImageDataGenerator(rescale=1. / 255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True)

# Testing Augmentation - Only Rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

# Generates batches of Augmented Image data
train_generator = train_datagen.flow_from_directory('./training_data/grayscale_256_top1_emd/train/',
                                                    target_size=(size, size),
                                                    batch_size=batch_size,
                                                    class_mode='binary',
                                                    color_mode='rgb',
                                                    shuffle=True)

# Generator for validation data
validation_generator = test_datagen.flow_from_directory('./training_data/grayscale_256_top1_emd/val/',
                                                        target_size=(size, size),
                                                        batch_size=batch_size,
                                                        class_mode='binary',
                                                        color_mode='rgb',
                                                        shuffle=True)

### Fit the model on Training data
ssmodel.fit(train_generator,
            epochs=15,
            validation_data=validation_generator,
            verbose=1)

# Evaluating model performance on Testing data
loss, accuracy = ssmodel.evaluate(validation_generator)

print("\nModel's Evaluation Metrics: ")
print("---------------------------")
print("Accuracy: {} \nLoss: {}".format(accuracy, loss))
