from keras.models import Sequential
import keras
import tensorflow as tf
from keras.layers import Conv2D, MaxPooling2D, Lambda, Dropout, Concatenate, GlobalAveragePooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
import pandas as pd
import sys
from sklearn import preprocessing

# some not-so-nice hacks to read separate module... means if this py file is ran as a script needs to be ran from its
# directory
sys.path.append(".")

from classes.DataGenerator import DataGenerator

size = 256
n_channels = 3

class imagenetSequentialMeta(tf.keras.Model):

    def __init__(self, size):

        super(imagenetSequentialMeta, self).__init__()

        # model1
        self.base = tf.keras.applications.MobileNet(weights='imagenet', include_top=False, input_shape=(size, size, n_channels))

        freeze_n = 30

        for layer in self.base.layers[:freeze_n]:
            layer.trainable = False
        for layer in self.base.layers[freeze_n:]:
            layer.trainable = True

        # self.conv1 = Conv2D(48, kernel_size=(3,3), padding="same")

        self.dense1 = Dense(1024)
        self.act1 = Activation('relu')
        self.pool1 = GlobalAveragePooling2D()

        self.flat = Flatten()

        # model2
        self.densem1 = Dense(12, activation='relu')
        self.densem2 = Dense(48, activation='relu')
        self.flat2 = Flatten()

        # combined
        self.concat = Concatenate()
        self.drop = Dropout(0.5)
        self.dense2 = Dense(768, activation='relu')

        self.drop2 = Dropout(0.3)
        self.dense3 = Dense(1, activation='sigmoid')

    def call(self, input_tensor):

        input_image = input_tensor[0]
        input_metafeatures = input_tensor[1]

        # model1
        x = self.base(input_image)

        # x = self.conv1(x)

        x = self.dense1(x)
        x = self.act1(x)
        x = self.pool1(x)

        x = self.flat(x)

        # model2
        x2 = self.densem1(input_metafeatures)
        x2 = self.densem2(x2)
        x2 = self.flat2(x2)

        xc = self.concat([x, x2])

        # combined
        xc = self.drop(xc)
        xc = self.dense2(xc)

        xc = self.drop2(xc)
        return self.dense3(xc)

    def build_graph(self):
        x = tf.keras.Input(shape=(size, size, 3))
        return tf.keras.Model(inputs=[x], outputs=self.call(x))


imMetaModel = imagenetSequentialMeta(size)
imMetaModel.compile(tf.optimizers.Adam(lr=1e-5), 'binary_crossentropy', metrics=['accuracy'])

dataset = pd.read_csv('/home/jan/Documents/python/git/filmnotfilm/training_data/training_csvs/training_file_20220607_grayscale_256_top1_emd.csv')

dataset.set_index('name', inplace=True)

partition = {}
partition['train'] = dataset[dataset['ml_kind'] == 'train'].index.values
partition['validation'] = dataset[dataset['ml_kind'] == 'val'].index.values

le = preprocessing.LabelEncoder()
encoded_labels = le.fit_transform(dataset.label)

labels = pd.Series(encoded_labels, index=dataset.index).to_dict()

# Parameters
params = {'dim': (size, size),
          'batch_size': 16,
          'n_classes': 2,
          'n_channels': n_channels,
          'shuffle': True}

# Generators
training_generator = DataGenerator(dataset, partition['train'], labels, **params)
validation_generator = DataGenerator(dataset, partition['validation'], labels, **params)

imMetaModel.fit(training_generator, validation_data=validation_generator, epochs=15, verbose=1)

# Evaluating model performance on Testing data
loss, accuracy = imMetaModel.evaluate(validation_generator)

print("\nModel's Evaluation Metrics: ")
print("---------------------------")
print("Accuracy: {} \nLoss: {}".format(accuracy, loss))