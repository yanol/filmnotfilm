# Import the Sequential model and layers
import keras
import tensorflow as tf
from keras.layers import Conv2D, MaxPooling2D, Lambda, Dropout
from keras.layers import Activation, Dropout, Flatten, Dense
from tensorflow.python.keras.layers import GlobalAveragePooling2D

size = 256
batch_size = 16

class imagenetSequential(tf.keras.Model):

    def __init__(self, size):

        super(imagenetSequential, self).__init__()

        self.base = tf.keras.applications.MobileNet(weights='imagenet', include_top=False, input_shape=(size, size, 3))

        freeze_n = 30

        for layer in self.base.layers[:freeze_n]:
            layer.trainable = False
        for layer in self.base.layers[freeze_n:]:
            layer.trainable = True

        self.pool1 = GlobalAveragePooling2D()

        self.dense1 = Dense(1024)
        self.act1 = Activation('relu')

        self.flat = Flatten()

        self.drop = Dropout(0.5)
        self.dense2 = Dense(768, activation='relu')

        self.drop2 = Dropout(0.3)
        self.dense3 = Dense(1, activation='sigmoid')

    def call(self, input_tensor):

        x = self.base(input_tensor)

        x = self.pool1(x)

        x = self.dense1(x)
        x = self.act1(x)

        x = self.flat(x)

        x = self.drop(x)
        x = self.dense2(x)

        x = self.drop2(x)
        return self.dense3(x)


imageSequential = imagenetSequential(size)

imageSequential.compile(loss='binary_crossentropy',
                        optimizer=tf.optimizers.Adam(lr=1e-5),
                        metrics=['accuracy'])

# Training Augmentation configuration
from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale=1. / 255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True)

# Testing Augmentation - Only Rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

# Generates batches of Augmented Image data
train_generator = train_datagen.flow_from_directory('./training_data/grayscale_256_top1_hpf/train/', target_size=(size, size),
                                                    batch_size=batch_size,
                                                    class_mode='binary',
                                                    color_mode='rgb',
                                                    shuffle=True)

# Generator for validation data
validation_generator = test_datagen.flow_from_directory('./training_data/grayscale_256_top1_hpf/val/',
                                                        target_size=(size, size),
                                                        batch_size=batch_size,
                                                        class_mode='binary',
                                                        color_mode='rgb',
                                                        shuffle=True)

### Fit the model on Training data
imageSequential.fit(train_generator,
                    epochs=15,
                    validation_data=validation_generator,
                    verbose=1)

# Evaluating model performance on Testing data
loss, accuracy = imageSequential.evaluate(validation_generator)

print("\nModel's Evaluation Metrics: ")
print("---------------------------")
print("Accuracy: {} \nLoss: {}".format(accuracy, loss))
