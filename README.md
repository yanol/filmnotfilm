# filmnotfilm #

## 1. Summary ##

### 1.1 Overview ###

The purpose of this project is to learn, explore and apply machine learning in a 'real world' situation. As an analogue photographer, for this project I have decided train a model which attempts to (first) tell apart analogue (film) vs. digital photographs and later add layers to possibly infer the brand of film.

### 1.2 Background ###

Differentiating between 35mm film and digitial photographs may seem impossible with the increasing ability to manipulate images and simulate different colour profiles and even image texture. 

A lot has been written about the dynamic range and signal-to-noise ratio in terms of the comparison between different film stocks and digital cameras [[1]](https://clarkvision.com/articles/film.vs.digital.summary1.html/). It is quite obvious that moden digital cameras and editing software are easily able to not just match (and go above) the dynamic range of film, but are able to replicate the hues, saturation and texture that is often desired.

Taking the below comparison image as an example [[2]](https://thedarkroom.com/film-vs-digital-comparison/):

![comparison](https://bitbucket.org/janlietava/filmnotfilm/raw/e06ac15a4d9a3c64e95b4bc5c2c65a909484c52f/images/examples/Photo-Comparison-Film-vs-Digital-1-1_512.png)

As can be read in more detail in the referenced article, the left image is shot on Velvia 50 film, while the right is a digital image taken using a Canon EOS 3. Though they may seem different above, post-processing can be used extensively. So, there is a need to find a differentiating factor which our machine learning algorithm will try and extract features of. 

#### 1.2.1 Identifying and emphasizing features of film ####

The first starting profile will be to try and train a model directly using rgb images to see if the extracted features are good enough, assuming that some of the factors mentioned above will be differentiable (hue, texture, saturation). 

However, there is a more unique identifier we can focus on - grain. Grain is the optical outcome of silver halide reactions which occur when film is exposed to light (and developed). The structure and randomess of noise is what separates it from pixelation in digital cameras. Consider the image below [[3]](https://www.richardphotolab.com/blog/post/film-grain-and-pixelation):

![comparison](https://www.richardphotolab.com/storage/app/media/blog/2018/10.18.18_Film-Grain-And-Pixelation/grain-pixelation-comparison.jpg)

Digital cameras may also have noise present in the pixelation due to dust and sensor imperfections, though this will generally not be present in a random structure (unlike film grain). 

#### 1.2.2 Similar research in other fields ####

Though not much recent research has looked into film grain detectior or measurement, there is a relatively large amount of research focusing on the identification of camera model purely from image data, mainly in the field of forensics ([[4]](https://arxiv.org/pdf/1808.06323.pdf), [[5]](https://arxiv.org/pdf/1603.01068.pdf), [[6]](https://hal.archives-ouvertes.fr/hal-01388975/document)).

Generally they utilise one of two approaches [[6]](https://hal.archives-ouvertes.fr/hal-01388975/document):

1. Using models built on the foundation 'sensor pattern noise' [[7]](https://ia.binghamton.edu/publication/FridrichPDF/double.pdf): this subsets a few different techniques, but focuses on using PRNU (photo response non-unformity; essentially the fact not all cells in the camera sensor will respond the same way, and this will be a unique signature) as well as radial distortion; the lenses being a unique key [[8]](https://www.researchgate.net/publication/26294909_Automatic_source_camera_identification_using_the_intrinsic_lens_radial_distortion).

2. Using machine learning, generally deep convolutational neural networks to identify and extract features from camera-image pairs. Often, feature selection is especially important, and hence a lot of research focuses on preparing images for feature extraction [[6]](https://hal.archives-ouvertes.fr/hal-01388975/document).

There is also a large amount of research that looks at de-noising film grain for optimisation in terms of improving encoding/compression [[9]](https://ieeexplore.ieee.org/document/5159451). One such approach is utilisng dicrete wavelet transformation (DWT) and thresholding to identify grain. For, example:
![dwt](https://i.imgur.com/cpUc0gN.png?1)

Wavelet decomposition can be used to capture localised change in different direction, as well as the location information. It is a piece of information we can provide to the model as a separate image file, or process into a singular value, such as looking at the variance (similar to Laplance variance for estimating blur of an image).

### 1.3 Optimising feature extraction ###

To experiment with, and optimise, feature extraction, several different pre-processing methods were utilised. 

Excepting model structure (see section 1.3.1 for how keras tuner was used to find optimal layer structure), the following were experimented with:

1. High-pass filter (HPF). To try and focus on image texture/grain rather than line boundaries or colour, a HPF of the following structure from Tuama, A., Comby, F. and Chaumont, M., 2016 [[6]](https://hal.archives-ouvertes.fr/hal-01388975/document) was utilised:
	
	```kernel = np.array([[-1, 2, -2, 2, -1],
	[2, -6, 8, -6, 2],
	[-2, 8, -12, 8, -2],
	[2, -6, 8, -6, 2],
	[-1, 2, -2, 2, 1]])```
	
2. Laplacian was also tested with the same goal as the above:

	```cv2.Laplacian(img, ddepth, ksize = kernel)```
	
3. Instead of image scaling, slicing was attempted. Since we want to focus on the grain/noise aspect of our image, instead of resizing, patching was utilised. Each image was split into 256x256 (or 224x224 depending on architecture) patches.

![slice](https://i.imgur.com/qMcFIvL.png?1)

4. Each patch was also evaluated, and top n patches were selected. The goal was to select patches from each image which do not just show (over)saturated parts of the image, but rather provide training data that will allow for better features to be extracted. The formula used was as follows [[9]](https://arxiv.org/abs/1809.00576):

![formula](https://i.imgur.com/WBbzIVU.png?1)

Where alpha, beta and gamma are constants, and mu and sigma are the mean and standard deviation respectively. We can see from the patched image above how areas of the image which have less high-frequency/saturation are given a higher score.

Example of processing:

| Base image | Laplacian | Wavelet (bior, LH) | EMD 1st residual |
| ---------  | --------- | ------------------ | ---------------- |
| ![base1](https://bitbucket.org/janlietava/filmnotfilm/raw/b237ee8807c84af3e47d650a88820bf6c916d6b5/images/examples/street_in_mexico_city_grayscale256.png) | ![lap](https://bitbucket.org/janlietava/filmnotfilm/raw/b237ee8807c84af3e47d650a88820bf6c916d6b5/images/examples/street_in_mexico_city_grayscale256_laplacian.png) | ![wav](https://bitbucket.org/janlietava/filmnotfilm/raw/b237ee8807c84af3e47d650a88820bf6c916d6b5/images/examples/street_in_mexico_city_grayscale256_wavelet.png) | ![emd](https://bitbucket.org/janlietava/filmnotfilm/raw/b237ee8807c84af3e47d650a88820bf6c916d6b5/images/examples/street_in_mexico_city_grayscale256_emd.png) |

### 1.3.1 Keras tuner ###

### 1.4 Training inputs ###

A combination of different parameters and models were attempted:

* 224x224/256x256 grayscale/RGB crop section of the image
* Signal to noise ratio (SNR)
* Horizontal/diagonal detail wavelet decomposition variance (using pywt library and 'bior1.3' setting)
* Dynamic range of the image
* transfer (densetnet/imagenet) learning base models
* EMD/wavelet transform
* picking best n patches

## 2. Results ##

Results for grayscale [256x256]* images are shown below [train:470, val:203], with varying processing applied:

| Image input** | Denset model | Simple sequential model | Imagenet base sequential model | Imagenet base sequential model with meta features |
| ----------- | ------------ | ----------------------- | ------------------------------ | ------------------------------------------------- |
| grayscale_256      |     0.57 (0.80)    |	 0.48 (0.69) 			   |		0.65 (0.62)				|						0.61 (0.65)						|
| grayscale_256_emd   |          |			    	|		0.62 (0.64)				|														|
| grayscale_256_hpf   |    0.54 (0.77)     |			       |		0.72 (0.56)				|						0.61 (1.27)						|
| grayscale_256_laplacian   |     |	 			  |			0.66 (0.61)				|														|
| grayscale_256_wavelet   |          |			  |			0.66 (0.63)				|														|
| grayscale_256_top1_wavelet   |          |				|	0.65 (0.65)				|														|	
| grayscale_256_top1_emd   |   0.58 (0.74)       |		0.48 (0.69)		|		0.74 (0.57)				|						0.67 (0.78)						|
| grayscale_256_top1_hpf   |          |		0.49 (0.69)		|		0.69 (0.58)				|														|


\* some base models required different input shape and in these situations, resizing could occur (eg. imagenet->224x224)

\** the format [topn] denotes where best patch extraction was applied and n patches were used for training (eg. top1 = highest rated patch only was used from the image)

### 2.1 RGB [224x224] simple sequential model ###
 
![simplesequential](https://i.imgur.com/PbnFRr7.png?2)


### 2.2 RGB [224x224] imagenet base sequential model ###

![imagenet](https://i.imgur.com/Q7w4ymN.png?1)


### 2.3 RGB [224x224] imagenet base 2 input model ###

![imagenet](https://i.imgur.com/9g5MZOD.png?1)

## 3. How do I get set up? ##

The following section describes how to reproduce the models. However, due to the nature of obtaining these images, they are not provided in the project.

### 3.1 Dependencies ###

The project dependencies can be installed using `pip install requirements.txt`

### 3.2 Summary of set up ###

There are three main sections of preparing and training the models:

1. `sample` in `main.py` - toggling this boolean and the username and password which has to be provided in a `config.py` file allow for scraping of reddit image posts for training.

2. `preprocess` in `main.py` - toggling this boolean decides whether the preprocess images and where to save the images, with what desired changes (size, sampling, resizing, high pass filtering, wavelet decomposition, EMD, laplacian,, etc).

3. `generate_training_file` in `main.py` - this boolean decides whether to generate a .csv file for training (which will generate meta-features like dynamic range, signal-to-noise ratio as well as wavelet decompositio variance). All models were training using a custom `DataGenerator` class and data was read from these .csv files.